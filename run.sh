rm -rf ../logs
j2 config/odoo.conf.j2 env.json  > config/odoo.conf
j2 docker-compose.yml.j2 env.json > docker-compose.yml
j2 nginx/nginx-config/nginx_template.conf.j2 env.json > nginx/nginx-config/nginx_template.conf
docker-compose up --build -d
