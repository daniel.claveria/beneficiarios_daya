{
    'name': 'Web Contact Search',
    'author': 'Daniel CLavería',
    'website': 'https://www.fundaciondaya.org',
    'license': 'AGPL-3',
    'depends': [
        "base",
        "contacts",
        "website",
        "l10n_cl",
        ],
    'contributors': [
        "Daniel Clavería daniel.claveria@gmail.com",
    ],
    'license': 'AGPL-3',
    'version': '14.0.1.0.0',
    'description': """
Search of contact from web site view
  """,
    'active': True,
    'data': [
        'security/groups_users.xml',
        'security/ir.model.access.csv',
        'reports/template_search_partner.xml',
        'wizards/wizard_search_partner.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False
}
