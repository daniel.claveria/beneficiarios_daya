
import io
import csv
import base64
import logging
import time
from datetime import datetime
from dateutil import relativedelta

from odoo import models, api, fields
import odoo.addons.decimal_precision as dp
from odoo.tools.translate import _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from odoo.exceptions import UserError

import logging

_logger = logging.getLogger(__name__)

class WizardSearchPartner(models.TransientModel):

    _name = 'wizard.search.partner'
    _description = 'Search of Partner by RUT'
    partner_rut = fields.Char(string="RUT beneficiario", size=12, required=True)
    
    def cargar_reporte_partner(self):
        data = {
            'model': 'wizard.search.partner',
            'form': self.read()[0]
        }
        temp = []

        partners = self.env['res.partner'].search([('vat','like', self.partner_rut)])

        for partner in partners:
            category = ''
            if partner.category_id:
                for c in partner.category_id:
                    category += c.name + ', '
            else:
                category = ''

            vals = {
                'name': partner.name,
                'category': category,
                'vat': partner.vat,
                'email': partner.email,
                'mobile': partner.mobile,
            }
            temp.append(vals)

        # Elimino totales, evaluar si se requiere
        #temp.append(vals)
        data['rows'] = temp
        data['now'] = datetime.now()
        data['reporte'] = "Beneficiario Fundación Daya"

        return self.env.ref('daya_web_contact_search.partner_report').report_action(self, data=data)


    
